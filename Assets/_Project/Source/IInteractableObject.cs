using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Coimbra;

public interface IInteractableObject
{
    int ActionType { get; set; }

    void OnActionBegin(Transform instigatorTransform);

    void OnActionEnd(Transform instigatorTransform);
}
