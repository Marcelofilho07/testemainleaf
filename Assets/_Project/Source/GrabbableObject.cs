using _Project.Source;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Coimbra;
using Coimbra.Services;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

public class GrabbableObject : Actor, IInteractableObject
{
    public int ActionType { get; set; }

    private Rigidbody _rigidbody;
    private FixedJoint _fixedJoint;
    private Collider _boxCollider;

    protected override void OnInitialize()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
        _boxCollider = GetComponent<Collider>();
    }

    private void FixedUpdate()
    {
        if (IsGrounded() || !_fixedJoint)
        {
            return;
        }

        _fixedJoint.connectedBody = null;
        _fixedJoint.breakForce = 0.001f;
        _fixedJoint.breakTorque = 0.001f;
        _rigidbody.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
    }

    public void OnActionBegin(Transform instigatorTransform)
    {
        Debug.Log("Action Begin!");
        _fixedJoint = gameObject.AddComponent(typeof(FixedJoint)) as FixedJoint;
        _fixedJoint!.connectedBody = instigatorTransform.GetComponent<Rigidbody>();
        _fixedJoint.breakForce = Mathf.Infinity;
        _fixedJoint.breakTorque = Mathf.Infinity;
        _rigidbody.constraints = RigidbodyConstraints.None;
    }

    public void OnActionEnd(Transform instigatorTransform)
    {
        _fixedJoint.connectedBody = null;
        _fixedJoint.breakForce = 0.001f;
        _fixedJoint.breakTorque = 0.001f;
        _rigidbody.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
    }

    private bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -UnityEngine.Vector3.up, _boxCollider.bounds.extents.y + 0.1f);
    }
}
