﻿using Coimbra.Services;
using UnityEngine;

namespace _Project.Source
{
    public interface IPlayerService : IService
    {
        Transform GetPlayerTransform();
    }
}
