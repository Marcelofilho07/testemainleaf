﻿# Main Leaf Framework

Core framework to be used when developing any project in Main Leaf.

It has dependencies on many third-party packages that we always include in the projects, and also includes asset packages that are commonly needed in most projects.

| Asset                                                                                                            | Version |
|------------------------------------------------------------------------------------------------------------------|---------|
| [DOTween Pro](https://assetstore.unity.com/packages/tools/visual-scripting/dotween-pro-32416)                    | 1.0.310 |
| [Easy Mobile Pro](https://assetstore.unity.com/packages/tools/integration/easy-mobile-pro-75476)                 | 2.18.1  |
| [EnhancedScroller](https://assetstore.unity.com/packages/tools/gui/enhancedscroller-36378)                       | 2.33.0  |
| [Fingers](https://assetstore.unity.com/packages/tools/input-management/fingers-touch-gestures-for-unity-41076)   | 3.0.8   |
| [Modern UI Pack](https://assetstore.unity.com/packages/tools/gui/modern-ui-pack-201717)                          | 5.4.6   |
| [Optimizers](https://assetstore.unity.com/packages/tools/utilities/optimizers-122247)                            | 2.2.0   |
| [Shapes](https://assetstore.unity.com/packages/tools/particles-effects/shapes-173167)                            | 4.1.3   |
| [Simple IAP System 2](https://assetstore.unity.com/packages/add-ons/services/billing/simple-iap-system-2-192362) | 5.1.1   |
| [SRDebugger](https://assetstore.unity.com/packages/tools/gui/srdebugger-console-tools-on-device-27688)           | 1.12.1  |
| [UMotion Pro](https://assetstore.unity.com/packages/tools/animation/umotion-pro-animation-editor-95991)          | 1.28p03 |
