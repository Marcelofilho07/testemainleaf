﻿using Coimbra.Services;
using UnityEngine;

namespace _Project.Source

{
    public interface ICameraService : IService
    {
        Vector3 GetForward();

        Vector3 GetRight();

        void SetCameraComponents(Vector3 position);
    }
}
