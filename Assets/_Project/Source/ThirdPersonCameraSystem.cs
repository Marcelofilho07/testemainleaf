﻿using Cinemachine;
using Coimbra.Services;
using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace _Project.Source
{
    public class ThirdPersonCameraSystem : ServiceActorBase<ThirdPersonCameraSystem, ICameraService>, ICameraService
    {
        public Vector3 GetForward()
        {
            return cinemachineVirtualCamera.transform.forward;
        }

        public Vector3 GetRight()
        {
            return cinemachineVirtualCamera.transform.right;
        }

        public void SetCameraComponents(Vector3 position)
        {
            if (_freeCamera)
            {
                SetTranspose();
                _freeCamera = false;
            }
            else
            {
                SetFollowPlayer();
                _freeCamera = true;
            }
        }

        [SerializeField]
        private Camera mainCamera;

        [SerializeField]
        private CinemachineVirtualCamera cinemachineVirtualCamera;

        private bool _freeCamera;
        protected override void OnInitialize()
        {

            mainCamera = Instantiate(mainCamera, new Vector3(0, 0, 0), Quaternion.identity);
            cinemachineVirtualCamera = Instantiate(cinemachineVirtualCamera, new Vector3(0, 0, 0), Quaternion.identity);
            cinemachineVirtualCamera.Follow = ServiceLocator.Shared.Get<IPlayerService>()!.GetPlayerTransform();
            cinemachineVirtualCamera.LookAt = ServiceLocator.Shared.Get<IPlayerService>()!.GetPlayerTransform();
            SetFollowPlayer();
            _freeCamera = true;
        }

        private void SetFollowPlayer()
        {
            ClearComponents();
            cinemachineVirtualCamera.AddCinemachineComponent<CinemachineFramingTransposer>();
            cinemachineVirtualCamera.AddCinemachineComponent<CinemachinePOV>();
        }

        // public void SetStationary(Vector3 cameraPosition)
        // {
        //     ClearComponents();
        //     cinemachineVirtualCamera.AddCinemachineComponent<CinemachineHardLookAt>();
        //     cinemachineVirtualCamera.transform.position = cameraPosition;
        // }

        private void SetTranspose()
        {
            ClearComponents();
            cinemachineVirtualCamera.AddCinemachineComponent<CinemachineTransposer>();
            cinemachineVirtualCamera.AddCinemachineComponent<CinemachineHardLookAt>();
        }

        private void ClearComponents()
        {
            while(cinemachineVirtualCamera.GetCinemachineComponent<CinemachineComponentBase>())
            {
                cinemachineVirtualCamera.DestroyCinemachineComponent<CinemachineComponentBase>();
            }
        }
    }
}
