using _Project.Source;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Coimbra;
using Cinemachine;
using Coimbra.Services;
using UnityEngine.Serialization;

public class GameMode : Actor
{
    [SerializeField]
    private PlayerSystem playerSystem;

    [SerializeField]
    private ThirdPersonCameraSystem cameraManager;
    protected override void OnInitialize()
    {
        playerSystem = Instantiate(playerSystem, new Vector3(0, 0, 0), Quaternion.identity);
        ServiceLocator.Shared.Set<IPlayerService>(playerSystem, true);

        cameraManager = Instantiate(cameraManager, new Vector3(0, 0, 0), Quaternion.identity);
        ServiceLocator.Shared.Set<ICameraService>(cameraManager, true);
    }
}
