﻿using Coimbra;
using Coimbra.Services;
using System;
using UnityEngine;

namespace _Project.Source
{
    public class PlayerSystem : ServiceActorBase<PlayerSystem, IPlayerService>, IPlayerService
    {
        public Transform GetPlayerTransform()
        {
            return playerActor.transform;
        }

        [SerializeField]
        private Actor playerActor;
        protected override void OnInitialize()
        {
            playerActor = Instantiate(playerActor, new Vector3(0, 0, 0), Quaternion.identity);
        }
    }
}
