using _Project.Source;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Coimbra;
using Cinemachine;
using Coimbra.Services;
using UnityEngine.Serialization;

public class Player : Actor
{
    [SerializeField]
    private float moveSpeed;

    [SerializeField]
    private float jumpForce;

    private PlayerInput _playerInput;
    private Rigidbody _playerRigidbody;
    private Collider _playerCollider;
    private Animator _playerAnimator;
    private Vector3 _inputMovement;
    private delegate void ActionDelegate(Transform instigator);

    private ActionDelegate _objActionBegin;
    private ActionDelegate _objActionEnd;
    private static readonly int Forward = Animator.StringToHash("Forward");
    private static readonly int Grab = Animator.StringToHash("OnGrab");
    private static readonly int Crouch = Animator.StringToHash("Crouch");
    private static readonly int Ground = Animator.StringToHash("OnGround");

    protected override void OnInitialize()
    {
        _playerInput = GetComponent<PlayerInput>();
        _playerRigidbody = GetComponent<Rigidbody>();
        _playerCollider = GetComponent<Collider>();
        _playerAnimator = GetComponentInChildren<Animator>();
    }

    private void FixedUpdate()
    {
        Vector3 unitInputMovement = _inputMovement.normalized;
        _playerRigidbody.AddForce(unitInputMovement * (moveSpeed * Time.deltaTime), ForceMode.Force);
        _playerAnimator.SetFloat(Forward, Vector3.Dot(unitInputMovement, transform.forward));
        if (_inputMovement.sqrMagnitude > 0.01f && !_playerAnimator.GetBool(Grab))
        {
            _playerRigidbody.MoveRotation(Quaternion.Slerp(_playerRigidbody.rotation, Quaternion.LookRotation(_inputMovement), 0.5f));
        }
    }

    public void OnWalk(InputAction.CallbackContext context)
    {
        ICameraService cameraService = ServiceLocator.Shared.Get<ICameraService>();
        Vector3 cameraForward = cameraService!.GetForward();
        Vector3 cameraRight = cameraService!.GetRight();

        cameraForward.y = 0.0f;
        cameraRight.y = 0.0f;

        _inputMovement = cameraForward * _playerInput.actions["Move"].ReadValue<Vector2>().y + cameraRight * _playerInput.actions["Move"].ReadValue<Vector2>().x;
    }

    public void OnCrouch(InputAction.CallbackContext context)
    {
        if (IsGrounded() && context.started)
        {
            _playerAnimator.SetBool(Grab, false);
            _playerAnimator.SetBool(Crouch, true);
        }
        else if(context.canceled)
        {
           _playerAnimator.SetBool(Crouch, false);
        }
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        if (!IsGrounded() || !context.started)
        {
            return;
        }

        _playerAnimator.SetBool(Grab, false);
        _playerAnimator.SetBool(Ground, false);
        _playerRigidbody.AddForce(new Vector3(0.0f, jumpForce, 0.0f), ForceMode.Impulse);
    }

    public void OnAction(InputAction.CallbackContext context)
    {
        if (IsGrounded() && context.started && _objActionBegin != null && !_playerAnimator.GetBool(Grab))
        {
            _playerAnimator.SetBool(Grab, true);
            _objActionBegin(transform);
        }
        else if(context.started && _objActionEnd != null)
        {
            _playerAnimator.SetBool(Grab, false);
            _objActionEnd(transform);
        }
    }

    private bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, _playerCollider.bounds.extents.y + 0.1f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (IsGrounded())
        {
            _playerAnimator.SetBool(Ground, true);
        }

        IInteractableObject interactableObject = collision.gameObject.GetComponent<IInteractableObject>();

        if (interactableObject == null)
        {
            return;
        }

        _objActionBegin = new ActionDelegate(interactableObject.OnActionBegin);
        _objActionEnd = new ActionDelegate(interactableObject.OnActionEnd);
    }

    private void OnCollisionExit(Collision collision)
    {
        IInteractableObject interactableObject = collision.gameObject.GetComponent<IInteractableObject>();

        if (interactableObject == null)
        {
            return;
        }

        _objActionEnd(transform);
        _objActionBegin = null;
        _objActionEnd = null;
    }
}
